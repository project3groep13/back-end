var mongoose = require('mongoose');

var RestaurantSchema = new mongoose.Schema({
    name: String,
    description: String,
    street: String,
    streetnumber: String,
    postalcode: String,
    town: String,
    scorevisited: Number,
    email: String,
    phonenumber: String
});

var Restaurant = mongoose.model('Restaurant', RestaurantSchema);
module.exports = Restaurant;