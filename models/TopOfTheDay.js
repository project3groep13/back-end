var mongoose = require('mongoose');

var TopOfTheDaySchema = new mongoose.Schema({
    lastupdated: {
        type: Date,
        default: Date.now
    },
    tipoftheday: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Tip'
    },
    restaurantoftheday: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Restaurant'
    },
    recipeoftheday: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Recipe'
    }
});

var TopOfTheDay = mongoose.model('TopOfTheDay', TopOfTheDaySchema);
module.exports = TopOfTheDay;