var mongoose = require('mongoose');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');

var UserSchema = new mongoose.Schema({
    firstname: String,
    lastname: String,
    username: {
        type: String,
        unique: true
    },
    hash: String,
    salt: String,
    email: {
        type: String,
        unique: true
    },
    birthdate: {
        type: Date,
        default: Date.now
    },
    gender: String,
    preferred: String,
    phonenumber: Number,
    score: Number,
    accepted: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Challenge'
    }],
    completed: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Challenge'
    }],
    suggested: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Challenge'
    }],
    restaurantsvisited: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Restaurant'
    }],
    facebook: Boolean,
    dailychallengestarted: {
        type: Date
    }
});

UserSchema.methods.setPassword = function(password) {
    this.salt = crypto.randomBytes(16).toString('hex');

    this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex');
};

UserSchema.methods.validPassword = function(password) {
    var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex');

    return this.hash === hash;
};

UserSchema.methods.getAge = function() {
    try {
        var diff = new Date - this.birthdate;
        var diffdays = diff / 1000 / (60 * 60 * 24);
        return Number(Math.floor(diffdays / 365.25));
    } catch (err) {
    	return new Error("Could not calculate age");
    }
}

UserSchema.methods.generateJWT = function() {

    // set expiration to 60 days
    var today = new Date();
    var exp = new Date(today);
    exp.setDate(today.getDate() + 60);

    return jwt.sign({
        _id: this._id,
        userName: this.userName,
        exp: parseInt(exp.getTime() / 1000),
    }, 'SECRET');
};

var User = mongoose.model('User', UserSchema);
module.exports = User;