var mongoose = require('mongoose');

var ChallengeSchema = new mongoose.Schema({
    title: String,
    description: String,
    difficulty: String,
    score: Number,
    localizeid: {
        type: Number,
        unique: true
    }
});

var Challenge = mongoose.model('Challenge', ChallengeSchema);

module.exports = Challenge;