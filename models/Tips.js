var mongoose = require('mongoose');

var TipSchema = new mongoose.Schema({
	title: String,
    description: String,
    shortdescription: String,
    rating: { type: Number, min: 0, max: 5 }
});

var Tip = mongoose.model('Tip', TipSchema);
module.exports = Tip;