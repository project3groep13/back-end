var mongoose = require('mongoose');

var ReceptenSchema = new mongoose.Schema({
	title: String,
	ingredients: [{
		ingredient: String,
		quantity: String
	}],
	preperation: [{
		step: Number,
		description: String
	}],
	image: String,
	difficulty: String,
	score: Number,
	localizeid: {
        type: Number,
        unique: true
    },
    duration: Number
});

var Recipe = mongoose.model('Recipe', ReceptenSchema);
module.exports = Recipe;