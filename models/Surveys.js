var mongoose = require('mongoose');

var SurveySchema = new mongoose.Schema({
	id: mongoose.Schema.Types.ObjectId,
	questions: [String],
	answers: [String],
	score: Number,
	accountFK: mongoose.Schema.Types.ObjectId
});