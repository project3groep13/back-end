var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var passport = require('passport');
var User = mongoose.model('User');
var users = require('./users.js');
var Tip = mongoose.model('Tip');
var tips = require('./tips.js');
var Challenge = mongoose.model('Challenge');
var TopOfTheDay = mongoose.model('TopOfTheDay');
var challenges = require('./challenges.js')
var Restaurant = mongoose.model('Restaurant');
var restaurants = require('./restaurants.js');
var Recipe = mongoose.model('Recipe');
var recipes = require('./recipes.js');
var jwt = require('express-jwt');
var auth = jwt({
    secret: 'SECRET',
    userProperty: 'payload'
});

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', {
        title: 'Backend Project 3 Groep 13'
    });
});

// REGISTER AND LOGIN
router.post('/login', users.login);
router.post('/register', users.register);
router.post('/registerfacebook', users.registerFacebook);
router.get('/initdb', recipes.filldb);

// OTHER CALLS
router.get('/restaurants', restaurants.getAll);
router.post('/restaurants/new', restaurants.create);
router.get('/tips', tips.getAll);
router.post('/tips/new', tips.create);
router.get('/tips/day', tips.getTipOfTheDay);
router.get('/tips/getrandom', tips.getRandomTip);
router.get('/challenges', challenges.getAll);
router.post('/challenges/new', challenges.create);
router.get('/recipes', recipes.getAll);
router.post('/recipes/new', recipes.create);
router.get('/recipes/day', recipes.getRecipeOfTheDay);


// USER SPECIFIC CALLS - POSTS
router.post('/addchallengeaccepted/:user/:challenge', auth, users.addChallengeAccepted);
router.post('/addchallengecompleted/:user/:challenge', auth, users.addChallengeCompleted);
router.post('/updateprofile/:user', auth, users.update);
router.post('/startdailychallenges/:user', auth, users.startDailyChallenges);
router.post('/addrestaurantvisited/:user/:restaurant', auth, users.addRestaurantVisited);
// USER SPECIFIC CALLS - GETS
router.get('/getchallenges/:user', auth, users.getChallengeIds);
router.get('/getpersonaldata/:user', auth, users.getPersonalData);
router.get('/getacceptedchallenges/:user', auth, users.getChallengesAccepted);
router.get('/getcompletedchallenges/:user', auth, users.getChallengesCompleted);
router.get('/getsuggestedchallenges/:user', auth, users.getSuggestedChallenges);
router.get('/getnewchallenges/:user', auth, users.getNewChallenges);
router.get('/getdaysinprogress/:user', auth, users.getDaysInProgress);
router.get('/getdailydata/:user', auth, users.getDailyData);

router.param('user', function(req, res, next, id) {
    var query = User.findById(id);

    query.exec(function(err, user) {
        if (err) {
            return next(err);
        }
        if (!user) {
            return next(new Error('Could not find user'));
        }

        req.user = user;
        return next();
    });
});

router.param('challenge', function(req, res, next, id) {
    var query = Challenge.findById(id);

    query.exec(function(err, challenge) {
        if (err) {
            return next(err);
        }
        if (!challenge) {
            return next(new Error('Could not find challenge'));
        }

        req.challenge = challenge;
        return next();
    });
});

router.param('restaurant', function(req, res, next, id) {
    var query = Restaurant.findById(id);

    query.exec(function(err, restaurant) {
        if (err) {
            return next(err);
        }
        if (!challenge) {
            return next(new Error('Could not find restaurant'));
        }

        req.restaurant = restaurant;
        return next();
    });
});

module.exports = router;