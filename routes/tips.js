var mongoose = require('mongoose');
var Tip = mongoose.model('Tip');
var TopOfTheDay = mongoose.model('TopOfTheDay');
var schedule = require('node-schedule');

var tips = {
    getAll: function(req, res, next) {
        Tip.find(function(err, tips) {
            if (err) {
                return next(err);
            }
            res.json(tips);
        });
    },

    getRandomTip: function(req, res, next) {
        var tip = findRandomTip();
        tip.then(function(tips) {
            var n = tips.length;
            var randomTip = tips[Math.floor(Math.random() * n)];
            return res.json(randomTip);
        });
    },

    create: function(req, res, next) {
        if (!req.body.title || !req.body.description || !req.body.rating) {
            return res.status(400).json({
                message: 'Please fill out all fields'
            });
        }
        /*
         title: String,
         description: String,
         shortdescription: String,
         rating: Number
         */

        var tip = new Tip();
        tip.title = req.body.title;
        tip.description = req.body.description;
        tip.shortdescription = req.body.shortdescription;
        tip.rating = req.body.rating;

        tip.save(function(err) {
            if (err) {
                return next(err);
            }
            return res.json(tip);
        });
    },

    getTipOfTheDay: function(res, res, next) {
        var topPromise = TopOfTheDay.find({}).exec();
        topPromise.then(function(top) {
            var promise = findTipByIdPromise(top.tipoftheday);
            promise.then(function(tip) {
                return res.json(tip);
            });
        });
    }
}

function findTipByIdPromise(id) {
    return Tip.find({
        _id: id
    }).exec();
}

function getTopPromise() {
    var top = TopOfTheDay.find({}).exec();
    return top;
}

function findTipOfTheDay(top) {
    var previousTip = top.tipoftheday;
    var promise =
        Recipe.find({
            _id: {
                $ne: previousTip
            }
        }).exec();
    return promise;
}

var j = schedule.scheduleJob({
    hour: 00,
    minute: 00
}, function() {
    var topPromise = getTopPromise();
    topPromise.then(function(top) {
        var promise = findTipOfTheDay(top[0]);
        promise.then(function(tips) {
            var n = tips.length;
            var randomTip = tips[Math.floor(Math.random() * n)];
            top[0].tipoftheday = randomTip;
            top[0].save(function(err) {
                if (err)
                    return new Error('Ooops! Something went wrong...');
                return top[0];
            });
        });
    })
});

function findRandomTip() {
    var promise =
        Tip.find({}).exec();
    return promise;
}

module.exports = tips;