var mongoose = require('mongoose');
var passport = require('passport');
var User = mongoose.model('User');
var Challenge = mongoose.model('Challenge');
var schedule = require('node-schedule');

var users = {
    login: function(req, res, next) {
        if (!req.body.username || !req.body.password) {
            return res.status(400).json({
                message: 'Please fill out all fields'
            });
        }

        passport.authenticate('local', function(err, user, info) {
            if (err) {
                return next(err);
            }

            if (user) {
                return res.json({
                    id: user._id,
                    username: user.username,
                    firstname: user.firstname,
                    lastname: user.lastname,
                    token: user.generateJWT()
                });
            } else {
                return res.status(401).json(info);
            }
        })(req, res, next);
    },

    register: function(req, res, next) {
        if (!req.body.password || !req.body.email || !req.body.firstname || !req.body.lastname) {
            return res.status(400).json({
                message: 'Please fill out all fields'
            });
        }

        var user = new User();
        if (!req.body.username) {
            user.username = req.body.email;
        } else {
            user.username = req.body.username;
        }

        user.setPassword(req.body.password)

        user.firstname = req.body.firstname;
        user.lastname = req.body.lastname;
        user.email = req.body.email;
        user.birthdate = new Date(req.body.birthdate);
        user.gender = req.body.gender;
        user.preferred = req.body.preferred;
        user.phonenumber = req.body.phonenumber;
        user.score = 0;
        user.facebook = false;

        user.save(function(err) {
            if (err) {
                return next(err);
            }

            return res.json({
                id: user._id,
                username: user.username,
                firstname: user.firstname,
                lastname: user.lastname,
                token: user.generateJWT()
            });
        });
    },

    registerFacebook: function(req, res, next) {
        if (!req.body.email || !req.body.firstname || !req.body.lastname) {
            return res.status(400).json({
                message: 'Please fill out all fields'
            });
        }

        var user = new User();
        if (!req.body.username) {
            user.username = req.body.email;
        } else {
            user.username = req.body.username;
        }
        if (req.body.password)
            user.setPassword(req.body.password);

        user.firstname = req.body.firstname;
        user.lastname = req.body.lastname;
        user.email = req.body.email;
        user.birthdate = new Date(req.body.birthdate);
        user.gender = req.body.gender;
        user.preferred = req.body.preferred;
        user.phonenumber = req.body.phonenumber;
        user.score = 0;
        user.facebook = true;

        user.save(function(err) {
            if (err) {
                return next(err);
            }
            console.log(user);
            return res.json({
                id: user._id,
                username: user.username,
                firstname: user.firstname,
                lastname: user.lastname
            });
        });
    },

    getDailyData: function(req, res, next) {
        if (!req.user) {
            return next(new Error('Could not find user'));
        }
        return res.json({
            accepted: req.user.accepted,
            completed: req.user.completed,
            suggested: req.user.suggested
                // Komt nog meer bij dan
        });
    },

    getPersonalData: function(req, res, next) {
        if (!req.user) {
            return next(new Error('Could not find user'));
        }
        var user = new User();
        user.username = req.user.username;
        user.firstname = req.user.username;
        user.lastname = req.user.lastname;
        user.email = req.user.email;
        user.phonenumber = req.user.phonenumber;
        user.gender = req.user.gender;
        user.birthdate = req.user.birthdate;
        user.preferred = req.user.preferred;
        user.score = req.user.score;
        user.accepted = req.user.accepted;
        user.completed = req.user.completed;
        user.suggested = req.user.suggested;
        user.facebook = req.user.facebook;
        user.dailychallengestarted = req.user.dailychallengestarted;

        return res.json(user);
    },

    getChallengeIds: function(req, res, next) {
        if (!req.user) {
            return next(new Error('Could not find user'));
        }
        var user = new User();
        user.accepted = req.user.accepted;
        user.completed = req.user.completed;
        user.suggested = req.user.suggested;
        return res.json(user);
    },

    getChallengesAccepted: function(req, res, next) {
        var promise = getChallengesAsJson(req, res, next, 'accepted');
        promise.then(function(challenges) {
            return res.json(challenges);
        });
    },

    getChallengesCompleted: function(req, res, next) {
        var promise = getChallengesAsJson(req, res, next, 'completed');
        promise.then(function(challenges) {
            return res.json(challenges);
        });
    },

    getSuggestedChallenges: function(req, res, next) {
        var promise = getChallengesAsJson(req, res, next, 'suggested');
        promise.then(function(challenges) {
            return res.json(challenges);
        });
    },

    update: function(req, res, next) {
        if (!req.user) {
            return next(new Error('Could not find user'));
        }
        if (req.body.username)
            req.user.username = req.body.username;
        if (req.body.password)
            req.user.setPassword(req.body.password);
        if (req.body.firstname)
            req.user.firstname = req.body.firstname;
        if (req.body.lastname)
            req.user.lastname = req.body.lastname;
        if (req.body.email)
            req.user.email = req.body.email;
        if (req.body.birthdate)
            req.user.setBirthDate(req.body.birthdate);
        if (req.body.gender)
            req.user.gender = req.body.gender;
        if (req.body.preferred)
            req.user.preferred = req.body.preferred;
        if (req.body.phonenumber)
            req.user.phonenumber = req.body.phonenumber;

        req.user.save(function(err) {
            if (err)
                return next(new Error('Ooops! Something went wrong...'));
            else
                return res.json(getPersonalData);
        });
    },

    addChallengeAccepted: function(req, res, next) {
        if (!req.challenge || !req.user)
            return next(new Error('Please fill in all fields'));
        if (req.user.accepted.indexOf(req.challenge._id) > -1) {
            return next(new Error('Already accepted this challenge'));
        }
        req.user.accepted.push(req.challenge._id);
        req.user.save(function(err) {
            if (err)
                return next(new Error('Ooops! Something went wrong...'));
            else
                return res.json({
                    accepted: req.user.accepted
                });
        });
    },

    addChallengeCompleted: function(req, res, next) {
        if (!req.challenge || !req.user)
            return next(new Error('Please fill in all fields'));
        if (req.user.completed.indexOf(req.challenge._id) > -1) {
            return next(new Error('Already completed this challenge'));
        }
        if (req.user.accepted.indexOf(req.challenge._id) > -1) {
            req.user.accepted.splice(req.user.accepted.indexOf(req.challenge._id), 1);
        } else {
            return next(new Error('You have not accepted this challenge!'));
        }
        req.user.completed.push(req.challenge._id);
        req.user.score = req.user.score + req.challenge.score;
        req.user.save(function(err) {
            if (err)
                return next(new Error('Ooops! Something went wrong...'));
            else
                return res.json({
                    completed: req.user.completed
                });
        });
    },

    getNewChallenges: function(req, res, next) {
        var randomChallenges = [];
        if (!req.user) {
            return next(new Error('Could not find user'));
        }
        console.log('user in function: ', req.user);
        var easy = findChallenge(req, 'easy', next);
        easy.then(function(challenges) {
            console.log('all easy: ', challenges);
            var n = challenges.length;
            if (n !== 0) {
                var randomChallenge = challenges[Math.floor(Math.random() * n)];
                randomChallenges.push(randomChallenge);
            }
        });

        var medium = findChallenge(req, 'medium', next);
        medium.then(function(challenges) {
            console.log('all medium: ', challenges);
            var n = challenges.length;
            if (n !== 0) {
                var randomChallenge = challenges[Math.floor(Math.random() * n)];
                randomChallenges.push(randomChallenge);
            }
        });

        var hard = findChallenge(req, 'hard', next);
        hard.then(function(challenges) {
            console.log('all hard: ', challenges);
            var n = challenges.length;
            if (n !== 0) {
                var randomChallenge = challenges[Math.floor(Math.random() * n)];
                randomChallenges.push(randomChallenge);
            }
            req.user.suggested = [];
            req.user.suggested = randomChallenges;
            req.user.save(function(err) {
                if (err)
                    return next(new Error('Ooops! Something went wrong...'));
                else {
                    console.log('suggested ', req.user.suggested);
                    return res.json(randomChallenges);
                }
            });
        });
    },

    startDailyChallenges: function(req, res, next) {
        if (!req.user) {
            return next(new Error('Could not find user'));
        }
        console.log('ISODATE start: ', req.user.dailychallengestarted);
        if (req.user.dailychallengestarted != undefined) {
            return next(new Error('Daily challenges already started!'));
        }
        req.user.dailychallengestarted = new Date();
        req.user.save(function(err) {
            if (err)
                return next(new Error('Ooops! Something went wrong...'));
            else {
                return res.json({
                    startDate: req.user.dailychallengestarted
                });
            }
        });
    },

    getDaysInProgress: function(req, res, next) {
        if (!req.user) {
            return next(new Error('Could not find user'));
        }
        console.log('ISODATE get: ', req.user.dailychallengestarted);
        if (!req.user.dailychallengestarted) {
            return next(new Error('Daily challenges not yet started!'));
        }

        var oneDay = 24 * 60 * 60 * 1000;
        var now = new Date();
        var nrOfDays = 1 + Math.round(Math.abs((req.user.dailychallengestarted.getTime() - now.getTime()) / (oneDay)));
        return res.json({
            totalDays: nrOfDays
        });
    },

    addRestaurantVisited: function(req, res, next) {
        if (!req.user) {
            return next(new Error('Could not find user'));
        }
         if (!req.restaurant) {
            return next(new Error('Could not find restaurant'));
        }
        req.user.score = req.user.score + req.restaurant.scorevisited;
        req.user.restaurantsvisited.push(req.restaurant._id);
        req.user.save(function(err) {
            if(err) {
                return next(new Error('Oops, something went wrong...'));
            }
            return res.json({
                newScore: req.user.score
            });
        });
    }
}

function findChallenge(req, diff, next) {
    var ongoingChallenges = [];
    ongoingChallenges.push(req.user.accepted);
    ongoingChallenges.push(req.user.completed);
    ongoingChallenges.push(req.user.suggested);
    var promise =
        Challenge.find({
            difficulty: diff,
            _id: {
                $not: {
                    $in: ongoingChallenges
                }
            }
        }).exec();
    return promise;
}

function findChallengeForUser(diff, user) {
    var ongoingChallenges = [];
    ongoingChallenges.push(user.accepted);
    ongoingChallenges.push(user.completed);
    ongoingChallenges.push(user.suggested);
    var promise =
        Challenge.find({
            difficulty: diff,
            _id: {
                $not: {
                    $in: ongoingChallenges
                }
            }
        }).exec();
    return promise;
}

function getChallengesAsJson(req, res, next, qualifier) {
    if (!req.user) {
        return next(new Error('Could not find user'));
    }

    var theChallenges;
    var result = [];

    switch (qualifier) {
        case "suggested":
            theChallenges = req.user.suggested;
            break;
        case "accepted":
            theChallenges = req.user.accepted;
            break;
        case "completed":
            theChallenges = req.user.completed;
            break;
        default:
            return next(new Error('Oops, something went wrong'));
    }
    console.log('challenges: ', theChallenges);
    var promise = Challenge.find({
        _id: {
            $in: theChallenges
        }
    }).exec();
    console.log('promise: ', promise);
    return promise;
}

function updateChallenges(user) {
    var randomChallenges = [];
    if (!user) {
        return new Error('Could not find user');
    }
    var easy = findChallengeForUser('easy', user);
    easy.then(function(challenges) {
        var n = challenges.length;
        if (n !== 0) {
            var randomChallenge = challenges[Math.floor(Math.random() * n)];
            randomChallenges.push(randomChallenge);
        }
    });

    var medium = findChallengeForUser('medium', user);
    medium.then(function(challenges) {
        var n = challenges.length;
        if (n !== 0) {
            var randomChallenge = challenges[Math.floor(Math.random() * n)];
            randomChallenges.push(randomChallenge);
        }
    });

    var hard = findChallengeForUser('hard', user);
    hard.then(function(challenges) {
        var n = challenges.length;
        if (n !== 0) {
            var randomChallenge = challenges[Math.floor(Math.random() * n)];
            randomChallenges.push(randomChallenge);
        }
        user.suggested = [];
        user.suggested = randomChallenges;
        user.save(function(err) {
            if (err)
                return new Error('Ooops! Something went wrong...');
            else {
                console.log('suggested ', user.suggested);
                return user.suggested;
            }
        });
    });
}

var j = schedule.scheduleJob({
    hour: 00,
    minute: 00
}, function() {
    User.find({}, function(err, results) {
        console.log('results ', results);
        results.forEach(function(user) {
            updateChallenges(user);
        });
    });
});

module.exports = users;