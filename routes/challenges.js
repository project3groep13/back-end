var mongoose = require('mongoose');
var Challenge = mongoose.model('Challenge');

var restaurants = {
    getAll: function(req, res, next) {
        Challenge.find(function(err, challenges) {
            if (err) {
                return next(err);
            }
            res.json(challenges);
        });
    },
    create: function(req, res, next) {
        if (!req.body.title || !req.body.description || !req.body.score || !req.body.difficulty || !req.body.localizeid) {
            return res.status(400).json({
                message: 'Please fill out all fields'
            });
        }
        /*
        title: String,
        description: String,
        score: Number
        */

        var challenge = new Challenge();
        challenge.title = req.body.title;
        challenge.description = req.body.description;
        challenge.difficulty = req.body.difficulty;
        challenge.score = req.body.score;
        challenge.localizeid = req.body.localizeid;

        challenge.save(function(err) {
            if (err) {
                return next(err);
            }
            return res.json(challenge);
        });
    }
}

module.exports = restaurants;