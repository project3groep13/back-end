var mongoose = require('mongoose');
var Recipe = mongoose.model('Recipe');
var TopOfTheDay = mongoose.model('TopOfTheDay');
var schedule = require('node-schedule');

var recipes = {
    getAll: function(req, res, next) {
        Recipe.find(function(err, results) {
            if (err) {
                return next(err);
            }
            res.json(results);
        });
    },

    create: function(req, res, next) {
        if (!req.body.title || !req.body.ingredients || !req.body.score || !req.body.difficulty || !req.body.localizeid) {
            return res.status(400).json({
                message: 'Please fill out all fields'
            });
        }
        /*
        title: String,
        description: String,
        score: Number
        */

        var recipe = new Recipe();
        recipe.title = req.body.title;
        recipe.ingredients = req.body.ingredients;
        recipe.difficulty = req.body.difficulty;
        recipe.score = req.body.score;
        recipe.localizeid = req.body.localizeid;

        recipe.save(function(err) {
            if (err) {
                return next(err);
            }
            return res.json(recipe);
        });
    },

    getRecipeOfTheDay: function(req, res, next) {
        var topPromise = getTopPromise();
        topPromise.then(function(top) {
            var promise = findRecipeByIdPromise(top.recipeoftheday);
            promise.then(function(recipe) {
                return res.json(recipe);
            });
        });
    },

    filldb: function(req, res, next) {
        var recipes = [{
            title: "Koude pastasalade met basilicum en olijfolie",
            image: "http://www.evavzw.be/sites/default/files/styles/header_image/public/recipe/header/pastasalade.JPG?itok=y0hBLJ1l",
            ingredients: [{
                ingredient: "korte pasta (bv. penne)"},{
                quantity: "500 gram"
            }, {
                ingredient: "paprika's"},{
                quantity: "2"
            }, {
                ingredient: "verse basilicum"},{
                quantity: ""
            }, {
                ingredient: "olijfolie"},{
                quantity: ""
            }, {
                ingredient: "tomaten (ontveld en geplet)"},{
                quantity: "3"
            }, {
                ingredient: "peper en zout"},{
                quantity: ""
            }],
            preperation: [{
                step: "1"},{
                description: "Deze salade moet koud opgediend worden, dus best de pasta een paar uur van tevoren koken."
            }, {
                step: "2"},{
                description: "De paprika's in kleine stukjes snijden en bij de koude pasta voegen. Olijfolie erdoor mengen, evenals de tomaten, en de verse basilicum, die in kleine stukjes gesneden is."
            }, {
                step: "3"},{
                description: "Kruiden met zout en peper."
            }],
            difficulty: 'easy',
            localizeid: 1,
            duration: 30
        }, {
            title: "Rodekoolsalade",
            image: "http://www.evavzw.be/sites/default/files/styles/header_image/public/recipe/header/Rodekoolsalade%20%281%29.jpg?itok=wVAWcB60",
            ingredients: [{
                ingredient: "rode kool"},{
                quantity: "1 kleine"
            }, {
                ingredient: "granny smith appel"},{
                quantity: "1"
            }, {
                ingredient: "rode ui"},{
                quantity: "1"
            }, {
                ingredient: "appelazijn"},{
                quantity: "2 el"
            }, {
                ingredient: "koolzaadolie"},{
                quantity: "2 el"
            }, {
                ingredient: "rietsuiker"},{
                quantity: "1 el"
            }, {
                ingredient: "veenbessen"},{
                quantity: "100 gram"
            }, {
                ingredient: "gemalen kruidnagel"},{
                quantity: "1 tl"
            }, {
                ingredient: "speculaaskruiden"},{
                quantity: "1 tl"
            }, {
                ingredient: "rode wijn"},{
                quantity: "6 el"
            }, {
                ingredient: "jam van rode bessen"},{
                quantity: "1 el"
            }, {
                ingredient: "dille"},{
                quantity: "4 takjes"
            }, {
                ingredient: "zeezout & gemalen peper"},{
                quantity: ""
            }],
            bereiding: [{
                step: "1"},{
                description: "Snipper de rode kool, de rode ui en de granny smithappel heel fijn en meng alles in een kom met de appelazijn, wat peper van de molen en zeezout. Laat alles een uurtje trekken in de koelkast en roer af en toe dooreen."
            }, {
                step: "2"},{
                description: "Breng de veenbessen aan de kook met de bruine suiker, rode wijn, rode bessenjam, kruidnagel en speculaaskruiden en laat zachtjes sudderen tot een compote. Laat afkoelen."
            }, {
                step: "3"},{
                description: "Meng de salade met de koolzaadolie en de veenbessen, schep er wat fijngesnipperde dille onder en breng verder op smaak met peper en zout."
            }],
            difficulty: 'easy',
            localizeid: 2,
            duration: 30
        }, {
            title: "Brownies met aquafaba",
            image: "http://www.evavzw.be/sites/default/files/styles/header_image/public/recipe/header/brownie.jpg?itok=Epj7icNr",
            ingredients: [{
                ingredient: "aquafaba kikkererwtenvocht"},{
                quantity: "10 el"
            }, {
                ingredient: "zelfrijzende bloem"},{
                quantity: "230 gram"
            }, {
                ingredient: "cacaopoeder"},{
                quantity: "70 gram"
            }, {
                ingredient: "rietsuiker"},{
                quantity: "400 gram"
            }, {
                ingredient: "bakpoeder"},{
                quantity: "1/2 tl"
            }, {
                ingredient: "zout"},{
                quantity: "1/2 tl"
            }, {
                ingredient: "sojamelk"},{
                quantity: "100 ml"
            }, {
                ingredient: "koolzaadolie of kokosolie"},{
                quantity: "280 ml"
            }, {
                ingredient: "vanille-extract"},{
                quantity: "1 el"
            }],
            bereiding: [{
                step: "1"},{
                description: "Verwarm de oven voor op 180°C."
            }, {
                step: "2"},{
                description: "Meng al de droge ingrediënten in een grote kom."
            }, {
                step: "3"},{
                description: "Giet alle natte ingrediënten in een kom en klop goed op met een klopper."
            }, {
                step: "4"},{
                description: "Voeg de natte ingrediënten bij de droge en meng voorzichtig door elkaar."
            }, {
                step: "5"},{
                description: "Giet het beslag in een ovenvorm en zet 20 minuten in de oven."
            }],
            difficulty: 'medium',
            localizeid: 3,
            duration: 60
        }];
        var reps = [];
        recipes.forEach(function(recipe) {
            var newRep = new Recipe();
            newRep.title = recipe.title;
            newRep.image = recipe.image;
            newRep.ingredients = recipe.ingredients;
            newRep.preperation = recipe.preperation;
            newRep.difficulty = recipe.difficulty;
            newRep.localizeid = recipe.localizeid;
            newRep.duration = recipe.duration;
            console.log(newRep);
            newRep.save(function(err) {
                console.log(err);
                if (err)
                    return next(new Error('oops'));
                reps.push(newRep);
            });
        });
        return res.json({success: "true"});
    }
}

function findRecipeByIdPromise(id) {
    return Recipe.find({
        _id: id
    }).exec();
}

function getTopPromise() {
    var top = TopOfTheDay.find({}).exec();
    return top;
}

function findRecipeOfTheDay(top) {
    var previousRecipe = top.recipeoftheday;
    var promise =
        Recipe.find({
            _id: {
                $ne: previousRecipe
            }
        }).exec();
    return promise;
}

var j = schedule.scheduleJob({
    hour: 00,
    minute: 00
}, function() {
    var topPromise = getTopPromise();
    topPromise.then(function(top) {
        var promise = findRecipeOfTheDay(top[0]);
        promise.then(function(recipes) {
            var n = recipes.length;
            var randomRecipe = recipes[Math.floor(Math.random() * n)];
            top[0].recipeoftheday = randomRecipe;
            top[0].save(function(err) {
                if (err)
                    return new Error('Ooops! Something went wrong...');
                return top[0];
            });
        });
    });

});


module.exports = recipes;