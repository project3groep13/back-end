var mongoose = require('mongoose');
var Restaurant = mongoose.model('Restaurant');

var restaurants = {
    getAll: function(req, res) {
        Restaurant.find(function(err, restaurants) {
            if (err) {
                return next(err);
            }
            res.json(restaurants);
        });
    },
    create: function(req, res) {
        if (!req.body.name || !req.body.description || !req.body.street || !req.body.streetnumber || !req.body.postalcode || !req.body.town) {
            return res.status(400).json({
                message: 'Please fill out all fields'
            });
        }
        /*
        name: String,
        description: String,
        street: String,
        streetnumber: String,
        postalCode: String,
        town: String,
        scorevisited: Number,
	email: String,
	phonenumber: String
        */

        var restaurant = new Restaurant();
        restaurant.name = req.body.name;
        restaurant.description = req.body.description;
        restaurant.street = req.body.street;
        restaurant.streetnumber = req.body.streetnumber;
        restaurant.postalCode = req.body.postalCode;
        restaurant.town = req.body.town;
        restaurant.scorevisited = req.body.scorevisited;
	restaurant.email  req.body.visited;
	restaurant.phonenumber = req.body.phonenumber;

        restaurant.save(function(err) {
            if (err) {
                return next(err);
            }
            return res.json(restaurant);
        });
    }
}

module.exports = restaurants;